<?php

require 'vendor/autoload.php';

use StubsGenerator\StubsGenerator;
use StubsGenerator\Finder;
use Symfony\Component\Filesystem\Filesystem;

$header = '
/**
 * Generated stub declarations for Wordpress.
 * https://upstatement.com/
 */';
$filename   = 'wordpress-stubs.php';
$include    = 'vendor/johnpbloch/wordpress-core';
$filesystem = new Filesystem();

// Generador del stub
$finder = Finder::create()->in($include)
	// ->notPath('assets')
	// ->notPath('lang')
	// ->notPath('wp-includes/spl-autoload-compat.php')
	// ->sortByName()
	;
$generator = new StubsGenerator(StubsGenerator::DEFAULT, ['nullify_globals' => true]);
$result        = $generator->generate($finder);

// Archivar el stub
$prettyPrinted = substr_replace($result->prettyPrint(), "<?php{$header}", 0, 5);
$filesystem->dumpFile($filename, $prettyPrinted);
